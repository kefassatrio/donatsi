from django.shortcuts import render, redirect
from .forms import CreateSaran
# Create your views here.
def tentang_kami(request):
    if(request.method == "POST"):
        form = CreateSaran(request.POST)
        if form.is_valid():
            form.save()
            return redirect("tentang-kami:about")
    else:
        form = CreateSaran()
    return render(request, "tentang-kami.html", {"form": form})