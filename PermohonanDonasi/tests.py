from django.test import TestCase, Client
from .models import Permohonan
import django.utils.timezone

# Create your tests here.
class PermohonanDonasiTest(TestCase):
    # def test_fail(self):
    #     self.fail("fail oiiii")
    
    def test_object_to_string(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
                                )
        self.assertEqual(str(permohonan), permohonan.nama_donasi)

    def test_default_image(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
                                )
        self.assertEqual(permohonan.image_donasi, "images/donatsi.png")

    def test_default_tanggal_pembuatan(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
                                )
        self.assertEqual(permohonan.tanggal_pembuatan, django.utils.timezone.now())

    def test_default_pilihan_kategori_alasan(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
                                )
        self.assertEqual(permohonan.pilihan_kategori_alasan, "kegiatan_sosial")

    def test_singkatan_deskripsi(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mauuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu maaaaaaaakannnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"
                                )
        self.assertEqual(permohonan.deskripsi_singkat(), permohonan.deskripsi_alasan[:50]+"...")

    ########################################
    def test_apakah_ada_url_slash_donasi(self):
        c = Client()
        response = c.get('/donasi/')
        self.assertEqual(response.status_code, 200) #200 = OK

    def test_apakah_ada_url_slash_donasi_daftar(self):
        c = Client()
        response = c.get('/donasi/')
        self.assertEqual(response.status_code, 200) #200 = OK

    def test_apakah_punya_form(self):
        c = Client()
        response = c.get('/donasi/daftar/')
        content = response.content.decode('utf8')
        self.assertIn('<form', content)

    def test_template_daftar_permohonan(self):
        c = Client()
        response = c.get('/donasi/daftar/')
        self.assertTemplateUsed(response, 'daftar-permohonan.html')
    
    def test_template_semua_permohonan(self):
        c = Client()
        response = c.get('/donasi/')
        self.assertTemplateUsed(response, 'semua-permohonan.html')
    
    def test_bikin_object(self):
        c = Client()
        response = c.post('/donasi/daftar/',{
            "nama_pemohon":"Kefas Satrio",
            "email": "kefassatrio@gmail.com",
            "bank": "BCA",
            "rekening": 12345678910,
            "nama_donasi": "beliin telur dong mau bikin donat",
            "target_uang": 2500000,
            "uang_terpenuhi": 0,
            "deadline": "2020-12-12",
            "deskripsi_alasan": "mau makan"
        })
        self.assertEqual(response.status_code, 200)