# Generated by Django 2.2.6 on 2019-10-13 05:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PermohonanDonasi', '0003_permohonan_image_donasi'),
    ]

    operations = [
        migrations.AlterField(
            model_name='permohonan',
            name='tanggal_pembuatan',
            field=models.DateField(auto_now_add=True),
        ),
    ]
