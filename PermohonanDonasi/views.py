from django.shortcuts import render, redirect
from .forms import CreatePermohonan
from KategoriDonasi.models import Kategori
from .models import Permohonan
from Comments.forms import add_comment
from Comments.models import data_comments

import datetime

# Create your views here.
def daftar_permohonan(request):
    form = CreatePermohonan()
    if request.method == "POST":
        form = CreatePermohonan(request.POST, request.FILES)
        if form.is_valid():
            permohonan = form.save(commit=False)
            permohonan.kategori_alasan = Kategori.objects.get(key_kategori=permohonan.pilihan_kategori_alasan)
            permohonan.urgency = permohonan.kategori_alasan.urgency
            permohonan.save()
            return redirect("homepage")
    return render(request, "daftar-permohonan.html", {'form': form})

def list_permohonan(request):
    query = request.GET.get("query")
    if query:
        objects = Permohonan.objects.filter(nama_donasi__contains=query)
    else:
        objects = Permohonan.objects.all()
    
    for permohonan in objects:
        if permohonan.deadline <= datetime.date.today():
            permohonan.delete()

    return render(request, 'semua-permohonan.html', {"objects": objects})

def detail_permohonan(request, id):
    permohonan = Permohonan.objects.get(id=id)
    if(request.method == "POST"):
        form = add_comment(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.permohonan = permohonan
            comment.save()
            return redirect("permohonan:detail-permohonan", id=id)
    
    form = add_comment()
    comments = data_comments.objects.filter(permohonan=permohonan).order_by("dateTime").reverse()
    return render(request, 'detail-permohonan.html', {"permohonan": permohonan, "comments": comments, "form": form})

