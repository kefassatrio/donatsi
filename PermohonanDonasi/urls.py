from django.contrib import admin
from django.urls import path, include
from .views import daftar_permohonan, list_permohonan, detail_permohonan

app_name = "permohonan"
urlpatterns = [
    path('', list_permohonan, name="list-permohonan"),
    path('<id>', detail_permohonan, name="detail-permohonan"),
    path('daftar/', daftar_permohonan, name="daftar-permohonan")
]
