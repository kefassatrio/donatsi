from django import forms
from . import models
from django.utils import timezone
from datetime import datetime

class CreatePemberian(forms.ModelForm):
    class Meta:
        model = models.Pemberian
        fields = "__all__"
        exclude = ['donasi']
