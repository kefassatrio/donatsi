from django.contrib import admin
from django.urls import path, include
from .views import daftar_pemberian

app_name = "pemberian"
urlpatterns = [
    path('<id>/beri-donasi', daftar_pemberian, name="daftar_pemberian"),
]
